
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
public class Day {
	// static variables
	
	//instance variables
	Event event;
	int numDay;
	int year;
	int month;
	
	Day(int x, int y, int z){
		numDay = x;
		month = y;
		year =z;
		event = new Event();
	}
	 public ArrayList<String> getEvent(){ // gets the list of events
		return event.getList();
	} 
	
	 public void setEvent(String eventX){ // adding events
		 event.setList(eventX);
	 }
	 
	public int getDay(){ // returns day
		return numDay;
	}
	public void setDay(int x){ // sets the day to its "day" in a month
		numDay = x;
	}
	public int getMonth(){
		return month;
	}
	public void setMonth(int x){
	 month = x;
	}
	public int getYear(){
		return year;
	}
	public void setYear(int x){
		year =x;
	}
	
	public static void main(String[] args){ // tests methods in Day class
		Day test = new Day(0,0,0); 
		test.setDay(10);
		int x = test.getDay();
		System.out.println(x);
		
		
	}
	
	
	
	
}

