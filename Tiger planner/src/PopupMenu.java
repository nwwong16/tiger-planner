import java.awt.*;
import java.awt.event.*;

import acm.graphics.*;

import javax.swing.*;
import javax.swing.border.EtchedBorder;

class PopupMenu extends JPanel implements ActionListener, ItemListener
 {
	public	static JPopupMenu popupMenu;
	public static Container pane1, pane2, addmenu;
	public static JPanel panel, panel2;
	public static JTextField jt;
	public static JButton submit;
	static JFrame f;
	static int charLimit = 50;
	static int charLimitDate = 10;
	static int charLimitTime = 5;
	public static JCheckBox notifications, weekly, hourly, daily;
	public static JLabel addl, name, date, time, repeat;
	//public static Day day;
	public static TigerCalendar day;
	public static JButton saveEvent, cancel, home;
	
	public PopupMenu()
	{
		setLayout(null);
		
		// Create some menu items for the popup
		JMenuItem menuFileViewDay = new JMenuItem( "View Day" );
		JMenuItem menuFileAddEvent = new JMenuItem( "Add Event" );

		// Create a popup menu
		popupMenu = new JPopupMenu( "Menu" );
		//setComponentPopupMenu(popupMenu);
		popupMenu.add( menuFileViewDay );
		popupMenu.add( menuFileAddEvent );
		
		//add popup menu to screen
		add( popupMenu );
		
		// Action and mouse listener support
		enableEvents( AWTEvent.MOUSE_EVENT_MASK );
		menuFileViewDay.addActionListener( this );
		menuFileAddEvent.addActionListener( this );
		
	}
	
	//listener for menu
 	public void processMouseEvent( MouseEvent event )
 	{
 		
 		if( event.isPopupTrigger())
 		{
 			popupMenu.show( event.getComponent(),
 								event.getX(), event.getY() );
 		}
 		super.processMouseEvent( event );
 	}
 	//opens a new window when you click on the menu option
 	public void newWindowViewDay(String s){
 		JFrame vd = new JFrame(s);
 		vd.setPreferredSize(new Dimension(500,500));
 		vd.setResizable(false);
 		
 		panel = new JPanel(null);
 		pane1 = vd.getContentPane();
 		pane1.setLayout(new BorderLayout());
 		
 		//add panel to container
 		pane1.add(panel);
 		
 		//String getMonth = day.getMonth();
 		//String getDay = String.valueOf(day.getDay());
 		//String getYr = String.valueOf(day.getYear());
 	
 		//JLabel dayLabel = new JLabel(getMonth);
 		
 		//panel.add(dayLabel);
 		//dayLabel.setBounds(40, 120, 440, 110);
 		
 		vd.pack();
 		vd.setVisible(true);	

 	}
 	
 	//opens a new window when you click on the menu option
 	public void newWindowAddEvent(String s){
 		
 		JFrame ae = new JFrame(s);
 		ae.setPreferredSize(new Dimension(500,500));
 		ae.setResizable(false);
 		
 		addl = new JLabel("Add New Event");
 		addl.setFont(new Font("Times New Roman",Font.BOLD, 36));
 		name = new JLabel("Event Name:");
 		date = new JLabel("Event Date:");
 		time = new JLabel("Event Time:");
 		repeat = new JLabel("Repeat:");
 		
 		JTextField nameTxt = new JTextField();
 		JTextField dateTxtStart = new JTextField(); //mm/dd/yyyy
 		JTextField dateTxtEnd = new JTextField(); 
 		JTextField timeTxtStart = new JTextField(); //hh:mm
 		JTextField timeTxtEnd = new JTextField();
 		
 		panel = new JPanel(null);
 	
 		pane1 = ae.getContentPane();
 		panel.setBackground(Color.WHITE);
 		pane1.setLayout(new BorderLayout());
 		
 		//add panel to container
 		pane1.add(panel);
 		
 		//add labels to panel
 		panel.add(addl);
 		panel.add(name);
 		panel.add(date);
 		panel.add(time);
 		
 		//add text fields
 		panel.add(nameTxt);
 		panel.add(dateTxtStart);
 		panel.add(dateTxtEnd);
 		panel.add(timeTxtStart);
 		panel.add(timeTxtEnd);
 		
 		//add button
 		saveEvent = new JButton("Save Event");
 		cancel = new JButton("Cancel");
 		home = new JButton("Home");
 		panel.add(saveEvent);
 		panel.add(cancel);
 		panel.add(home);
 		
 		//add graphics 
 		panel.setBorder(BorderFactory.createLineBorder(Color.BLACK,3,true));
 		
 		//add check box for notifications
 		notifications = new JCheckBox("Notifications?");
 		notifications.setSelected(false);
 		notifications.addItemListener(this);
 		panel.add(notifications);
 		
 		//place labels on panel
 		//setBounds(left/right, up/down, width, height);
 		addl.setBounds(40, 10, 396, 55);
 		name.setBounds(40, 40, 440, 110);
 		date.setBounds(40, 80, 440, 110); 
 		time.setBounds(40, 120, 440, 110); 
 		
 		//location of button
 		saveEvent.setBounds(125, 350, 100, 75);
 		cancel.setBounds(225, 350, 100, 75);
 		home.setBounds(375, 10, 75, 50);
 		
 		//set locations of entries
 		nameTxt.setBounds(125,85,250,25);
 		dateTxtStart.setBounds(125,125,100,25);
 		dateTxtEnd.setBounds(225,125,100,25);
 		timeTxtStart.setBounds(125,165,100,25); //hh:mm
 		timeTxtEnd.setBounds(225,165,100,25);

 		//set locations of check boxes
 		notifications.setBounds(40, 200,200,100);
	 		
 		ae.pack();
 		ae.setVisible(true);
 	}
 	
 	public void itemStateChanged(ItemEvent e){
 		Object source = e.getItemSelectable();
 		
 		if(source == notifications){
 			//if item selected
 			System.out.println("Notifications Selected");
 			weekly = new JCheckBox("Weekly");
 	 		weekly.setSelected(false);
 	 		weekly.addItemListener(this);
 	 		panel.add(weekly);
 	 		weekly.setBounds(80, 250,200,100);
 		}
 		if(e.getStateChange() == ItemEvent.DESELECTED){
 			//if item deselected
 			System.out.println("Notifications Deselected");
 		}
 	}
 	
	public void actionPerformed(ActionEvent event)
 	{
 		String cmd = event.getActionCommand();
 		// Add action handling code here
 		
 		if(cmd.equals("View Day")){
 			newWindowViewDay(cmd);
 		}
 		if(cmd.equals("Add Event")){	
 			newWindowAddEvent(cmd);
 		}  
 		 
 		
 		System.out.println( event );
 		System.out.println(event.getActionCommand());
 	}
	/*
	public static void main( String args[] )
	{	
		// Create an instance of the test application
		JFrame f = new JFrame();
		f.setPreferredSize(new Dimension(500,500));
		f.setContentPane(new PopupMenu());
		f.setVisible(true);
		f.setSize( 310, 130 );
		f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
	}*/

}