
import java.io.*;
import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;

import javax.swing.text.html.HTMLDocument.Iterator;
public class LoadEvents extends HashMap<Integer,Day> {

	
// static variables

//instance variables	
	HashMap<String, Day> map;

	
LoadEvents(){
	try {
		
		BufferedReader reader = new BufferedReader(new FileReader("eventlist"));// reads the file eventlist from source folder
		String line = new String();
		Day newDay = null;
		map  = new HashMap<String,Day>();
		while ((line = reader.readLine()) != null) {
		    System.out.println(line);
		    if (line.substring(0,3).equals("day")){
		    	newDay = new Day(0,0,0);
		    	newDay.setDay(Integer.valueOf(line.substring(3,5)));
		    	newDay.setMonth(Integer.valueOf(line.substring(5,7)));
		    	newDay.setYear(Integer.valueOf(line.substring(7,11)));
		    	
		    	map.put(newDay.getDay()+"-"+newDay.getMonth()+"-"+newDay.getYear(),newDay);
		    	System.out.println(map.keySet());
		    	
		    }
		    else{
		    	newDay.setEvent(line);
		    }
		    
		}
		reader.close();
	} catch (FileNotFoundException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	} catch (IOException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}

	
}

public void addMapOject(int day, int month, int year, Day addDay){
	map.put("day"+"-"+"month"+"-"+"year", addDay);
}

public Day getDayOb(String x){
	return map.get(x);
}


public void printMap(int day, int month, int year){
	System.out.println("this is a day "+ map.get(day+"-"+month+"-"+year));
	System.out.println(day+"-"+month+"-"+year);
}

public void showKeys(){
	System.out.println(map.keySet());
}
public void showEvents(String x){
	Day staticDay=new Day(0,0,0);
	ArrayList<String> list = new ArrayList<String>();
	staticDay= map.get(x);
	list=staticDay.getEvent();
	for(int i=0; i < list.size();i++){
		System.out.println(list.get(i));
	}
	
	
}


public void testSet(){   // this was to read out the file
	
	Set <String> keys = map.keySet(); // gets the keySet and puts it in Set to read from
	Day staticDay= new Day(0,0,0);
	ArrayList<String> list= new ArrayList<String>();
	File file = new File("eventlist");
	FileWriter fw = null;
	try {
		fw = new FileWriter(file.getAbsoluteFile());
	} catch (IOException e1) {
		// TODO Auto-generated catch block
		e1.printStackTrace();
	}
	BufferedWriter bw = new BufferedWriter(fw);
	 for (String s:keys){   // one by one reads through the set of keys
		 System.out.println(s);
		 staticDay= map.get(s);
		 list=staticDay.getEvent();
			try {
				bw.write("day"+staticDay.getDay()+staticDay.getMonth()+staticDay.getYear()+"\n"); // creates base line for reference to read in the file
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		for (int x=0; x < list.size(); x++){
			try {
				bw.write(list.get(x)+"\n");
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

	 }
	 try {
		bw.close();
	} catch (IOException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
}



public static void main(String[] args){
	LoadEvents test = new LoadEvents();
	Day testDay = new Day(0,0,0);
	ArrayList<String> list= new ArrayList <String>();
	test.printMap(24,12,2014);
	System.out.println(24+"-"+12+"-"+2014);
	test.showKeys();
	list.add("stuff");
	test.showEvents(24+"-"+12+"-"+2014);
	testDay =test.getDayOb(24+"-"+12+"-"+2014);
	System.out.println(testDay.getDay());
	list=testDay.getEvent();
	System.out.println(list.get(0));
//	Day daySurprise = new Day(15,12,2014);   // this is for Demo
//	daySurprise.setEvent("Surpriseeeeeeee");
//	daySurprise.setEvent("didn't see that coming right?");
//	test.addMapOject(daySurprise.getDay(),daySurprise.getMonth(),daySurprise.getYear(),daySurprise); // end of Demo
	test.testSet();
	
	
//	System.out.println(testDay.getEvent());

	/*for (int i=0; i < list.size(); i++){
		System.out.println(list.get(i));
	}
	*/
	
}


}
 

 
