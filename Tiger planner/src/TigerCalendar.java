/*********************************************************************
 * File: TigerCalendar.j
 * Description: This file is the TigerCalendar class that handles
 * 				the display of the calendar itself and the labels.
************************************************************************/

import java.awt.Color;
import java.util.*;
import java.util.Calendar;

import acm.graphics.GLabel;
import acm.graphics.GLine;
import acm.program.*;

public class TigerCalendar extends GraphicsProgram{
	
	public static final int PROGRAM_WIDTH = 1000;
	public static final int PROGRAM_HEIGHT = 1000;
	
	public static final String lABEL_FONT = "Times-Italic-52";
	
	public void init() {
		setSize(PROGRAM_WIDTH, PROGRAM_HEIGHT);
	}
	
	public void run(){
		//draw label on Calendar
		//drawCalendarLabels();
	}
	
	/*********************************************************************
	 * Method: getMonthString
	 * Parameters: integer
	 * Description: maps an integer to a string representing the month
	 * 				key is the integer, value is the string of the month
	 * 				returns current month
	 * Returns: String
	************************************************************************/
	public String getMonthString(int m){
		//method that displays the month and year on the calendar GUI
		
		//hashtable maps the integer values to respective month
		Hashtable<Integer, String> numbers = new Hashtable<Integer, String>();
		numbers.put(0, "January");
		numbers.put(1, "February");
		numbers.put(2, "March");
		numbers.put(3, "April");
		numbers.put(4, "May");
		numbers.put(5, "June");
		numbers.put(6, "July");
		numbers.put(7, "August");
		numbers.put(8, "September");
		numbers.put(9, "October");
		numbers.put(10, "November");
		numbers.put(11, "December");
		
		//System.out.println(numbers.get(m));
		
		return numbers.get(m);
	}
	
	/*********************************************************************
	 * Method: getMonth
	 * Parameters: None
	 * Description: gets month from Calendar class in Java Library (0-15/ Jan-Dec)
	 * Returns: Integer
	************************************************************************/
	public int getMonth() {
		//month ranges from 0-11, Jan = 0 and Dec = 11 
		Calendar cal = new GregorianCalendar();
		int month = cal.get(Calendar.MONTH);
		return month;
	}
	
	/*********************************************************************
	 * Method: getDayOfWeek
	 * Parameters: None
	 * Description: Returns current day of week ranges 1-7, sun = 1 and sat = 7 
	 * Returns: Integer
	************************************************************************/
	public int getDayOfWeek(){
		Calendar cal = new GregorianCalendar();
		int dayOfWeek = cal.get(Calendar.DAY_OF_WEEK);
		return dayOfWeek;
	}
	
	/*********************************************************************
	 * Method: getDay
	 * Parameters: None
	 * Description: Returns current day of month, ranges 1-31
	 * Returns: Integer
	************************************************************************/
	public int getDay() {
		Calendar cal = new GregorianCalendar();
		int dayOfMonth = cal.get(Calendar.DAY_OF_MONTH);
		return dayOfMonth;
	}
	
	/*********************************************************************
	 * Method: getYear
	 * Parameters: None
	 * Description: Returns current year
	 * Returns: Integer
	************************************************************************/
	public int getYear() {
		Calendar cal = new GregorianCalendar();
		int year = cal.get(Calendar.YEAR);
		return year;
	}
	
	/*********************************************************************
	 * Method: drawCalendarLabels
	 * Parameters: None
	 * Description: Creates the current month label and places it on 1000x1000 app
	 * Returns: None
	************************************************************************/
	public void drawCalendarLabels(){
		TigerCalendar mycal = new TigerCalendar();
		String currMonth = mycal.getMonthString(mycal.getMonth());
		int currYear = mycal.getYear();
		
		GLabel monthLabel = new GLabel(currMonth+" "+currYear,PROGRAM_WIDTH/2.5,PROGRAM_HEIGHT/10);
		//GLabel monthLabel = new GLabel(currMonth+" "+currYear,50,50);
		
		monthLabel.setFont(lABEL_FONT);
		monthLabel.setColor(Color.BLUE);
		add(monthLabel);
	}

	
}
