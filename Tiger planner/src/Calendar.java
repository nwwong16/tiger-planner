/*import acm.program.*;
public class Calendar extends GraphicsProgram {
}*/

import java.awt.*;
//import java.awt.event.ActionEvent;
//import java.awt.event.ActionListener;
//import javax.swing.event.*;
import java.awt.event.*;
//import java.awt.image.*;
import javax.swing.*;
import javax.swing.table.*;
import java.util.*;



public class Calendar{

	static int realYear, realMonth, realDay, currentYear, currentMonth;
	static JFrame mainFrame;
	static Container pane;
	static JLabel monthYearLabel;
	static JTable calendarTable; 
	static DefaultTableModel defCalendarTable; //Default Grid
	static JScrollPane scrollCalendarTable; 
	static JPanel calendarPanel; //Panel
	static Font bigText = new Font("Times New Roman", Font.BOLD+Font.ITALIC, 48); //set font for monthYearLabel
	static Font smallText = new Font("Times New Roman", Font.BOLD+Font.ITALIC, 14); //set font for today button
	static JButton next, previous, today, events;

	public static void updateCalendar(int month, int year){ //refreshing page

		String[] months =  {"January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"};
		int numOfDays, startingDayOfMonth;

		next.setEnabled(true);
		previous.setEnabled(true);
		today.setEnabled(true);

		monthYearLabel.setText(months[month]+" "+year); //set the label for month & year according to what month it is
		monthYearLabel.setFont(bigText);
		monthYearLabel.setBounds(352-monthYearLabel.getPreferredSize().width/2, 55, 396, 55); //for alignment of the month label


		for (int i=0; i<6; i++){ //to empty the table
			for (int j=0; j<7; j++){
				defCalendarTable.setValueAt(null, i, j);
			}
		}
		
		

		GregorianCalendar cal = new GregorianCalendar(year, month, 1); //declaring the calendar
		numOfDays = cal.getActualMaximum(GregorianCalendar.DAY_OF_MONTH); //getting first day of the month
		startingDayOfMonth = cal.get(GregorianCalendar.DAY_OF_WEEK); //getting the first day of the week


		for (int i = 1; i <= numOfDays; i++){ //creating the grid 
			int row = new Integer((i + startingDayOfMonth - 2)/7);
			int column = (i + startingDayOfMonth - 2)%7;
			defCalendarTable.setValueAt(i, row, column); //setting days within each cell
		}
		


		calendarTable.setDefaultRenderer(calendarTable.getColumnClass(0), new calendarTableRenderer());
	}

	static class next_Action implements ActionListener{
		public void actionPerformed (ActionEvent e){
			if (currentMonth == 11){ //if we're at the 12th month, December 
				currentMonth = 0; //go to the first month, January
				currentYear += 1; //and then add 1, currentYear = currentYear + 1
			}
			else{ 
				currentMonth += 1; //going to next month, if month isn't the edge case of December 
			}

			updateCalendar(currentMonth, currentYear); //updating page
		}
	}




	static class previous_Action implements ActionListener{ 
		public void actionPerformed (ActionEvent e){
			if (currentMonth == 0){ //if we're at the 1st month, January (edge case)
				currentMonth = 11; //then go to the 12th month, December
				currentYear -= 1; //currentYear = currentYear - 1
			}
			else{ //going to previous month, if not at the edge case of January
				currentMonth -= 1;
			}
			updateCalendar(currentMonth, currentYear); //updates the page to reflect dates in current month
		}

	}

	

	static class today_Action implements ActionListener{//implement today button to return to current year and month from any different month/year
		public void actionPerformed (ActionEvent e){
			if(currentMonth != realMonth && currentYear != realYear)
			{
				currentMonth = realMonth;
				currentYear = realYear;
			}
			else if(currentMonth != realMonth && currentYear == realYear)
			{
				currentMonth = realMonth;
				//currentYear = realYear;
			}
			else if(currentMonth == realMonth && currentYear != realYear)
			{
				//currentMonth = realMonth;
				currentYear = realYear;
			}

			updateCalendar(currentMonth, currentYear); //updates the page to reflect dates in current month
		}
	}



	static class calendarTableRenderer extends DefaultTableCellRenderer{ //setting the design of the grid

		public Component getTableCellRendererComponent (JTable table, Object value, boolean selected, boolean focused, int row, int column){
			super.getTableCellRendererComponent(table, value, selected, focused, row, column);
			if (column == 0 || column == 6){ //weekend (Saturday and Sunday)
				setBackground(new Color(255, 153, 51)); //orange
			}
			else{ //5-day work week
				setBackground(new Color(224, 224, 224)); //light gray
			}
			if (value != null){ //feature: to highlight current day
				if (Integer.parseInt(value.toString()) == realDay && currentMonth == realMonth && currentYear == realYear){ 
					setBackground(new Color(160, 160, 160));
				}
			}
			setForeground(Color.black);
			return this; 
		}
	}



	public static void main (String args[]){

		//error catching
		try {UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());}
		catch (ClassNotFoundException e) {}
		catch (InstantiationException e) {}
		catch (IllegalAccessException e) {}
		catch (UnsupportedLookAndFeelException e) {}


		mainFrame = new JFrame ("Tiger Planner"); //creating the grid...
		mainFrame.setSize(750, 720); //size in pixels
		pane = mainFrame.getContentPane(); //to get the contents pane
		pane.setLayout(null); //to have empty layout
		mainFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); //to exit with "X"


		monthYearLabel = new JLabel ("January"); //clearing null pointer exception, declaring to default
		defCalendarTable = new DefaultTableModel(){public boolean isCellEditable(int rowIndex, int colIndex){return false;}};
		calendarTable = new JTable(defCalendarTable);
		scrollCalendarTable = new JScrollPane(calendarTable);
		calendarPanel = new JPanel(null);
		next = new JButton (">"); //next
		next.setFont(bigText);
		previous = new JButton ("<"); //previous
		previous.setFont(bigText);
		today = new JButton ("Today");
		today.setFont(smallText);
		
		
		//Icon home = new ImageIcon(Calendar.class.getResource("home.png"));
		//JButton homeButton =  new JButton("Home", home);



		pane.add(calendarPanel); //adding buttons, labels
		calendarPanel.add(monthYearLabel);
		calendarPanel.add(scrollCalendarTable);
		calendarPanel.add(next);
		calendarPanel.add(previous);
		calendarPanel.add(today);
		//calendarPanel.add(homeButton);
		//calendarTable.add(events); //setting days within each cell


		
		//Grid layout
		calendarPanel.setBounds(0, 0, 750, 750); //working area of calendar grid display
		monthYearLabel.setBounds(352-monthYearLabel.getPreferredSize().width/2, 110, 440, 110); //centering the month label
		scrollCalendarTable.setBounds(22, 110, 660, 550); 
		next.setBounds(585, 55, 96, 45); //next button //Left/right, Up/down, Width, Height
		previous.setBounds(22, 55, 96, 45); //previous button 
		today.setBounds(352-today.getPreferredSize().width/2, 20, 80, 30);
		//(homeButton).setBounds(352-homeButton.getPreferredSize().width/2, 50, 70, 40);


		mainFrame.setVisible(true); //to display the frame


		GregorianCalendar cal = new GregorianCalendar(); //Create calendar
		realDay = cal.get(GregorianCalendar.DAY_OF_MONTH); //Get day
		realMonth = cal.get(GregorianCalendar.MONTH); //Get month
		realYear = cal.get(GregorianCalendar.YEAR); //Get year
		currentMonth = realMonth; //check if correct month
		currentYear = realYear; //check if correct year



		String[] dayTitle = {"Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"}; //day dayTitle string, called by addColumn

		for (int i=0; i<7; i++){
			defCalendarTable.addColumn(dayTitle[i]); //adding the day titles to column header
		}

		calendarTable.getParent().setBackground(calendarTable.getBackground()); //setting the background


		calendarTable.getTableHeader().setResizingAllowed(false); //to not allow resizing
		calendarTable.getTableHeader().setReorderingAllowed(false); //to not allow resizing
		//calendarTable.setRowHeight(70); //height
		calendarTable.setRowHeight(87); //height of rows

		defCalendarTable.setColumnCount(7); //number of columns
		defCalendarTable.setRowCount(6); //number of rows

		next.addActionListener(new next_Action());
		previous.addActionListener(new previous_Action());
		today.addActionListener(new today_Action());
		//(homeButton).addActionListener(new home_Action());




		updateCalendar (realMonth, realYear); //updates calendar to reflect current month being displayed
	}
}








